# Flask chessgame

> Clamadieu-Tharaud Adrien
> Gélineau Benjamin
> Joyet Hugo

## Explaination

- The objective of the project was to create our own chess website made with flask and docker

## prerequisites

```
docker
flask
Python in 3.X
```

## Setup

- First you will need to git clone the project:

```
git clone https://gitlab.com/benji.gelineau/chessgame.git
```

- Then you just need to do:

```
pip install -r requirements.txt
docker-compose up -d
# wait 2 or 3 minutes for the docker container to be created
python -m flask run
```

## Features

### CI/CD

- We have a CI/CD pipeline that is activated when there is a change in the code and that change is pushed to the git repository. This pipeline performs tests, builds the project and deploys it. Thanks to this, we don't need to restart the server, the changes are made and the users just have to refresh the page.

### Register/login

- First of all we have a login/register page that will allowed users to create accounts on
  ![](https://i.imgur.com/zdlNJOJ.png)

### Game Menu

- when you will be logged in, you will arrive on the game menu, you will be able to determine if you want to play versus an ia or against an another local player. You have also the possibility to manage your options and logout.
  ![](https://i.imgur.com/csS8UOq.png)

### Home

- And finally you have the game
  ![](https://i.imgur.com/pyRWZhe.png)
